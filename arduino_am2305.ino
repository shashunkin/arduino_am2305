/* Aosong AM2305 Sensor + Arduino Nano
 * 
 * 
 *  
 *  * Aosong AM2305:
 * Read temperature and humidity single-bus interface
 * VCC       +3.3 v
 * SDA       4.7 - 5.1kΩ pull-up resistor
 * GND
 * 
 * 
 * 
 *  * Many Thanks for help:
 * 1. https://rubenlaguna.com/post/2008-10-15-arduino-sleep-mode-waking-up-when-receiving-data-on-the-usart/
 * 2. http://donalmorrissey.blogspot.ru/2010/04/sleeping-arduino-part-3-wake-up-via.html
 * 
 * 
 * 
 * Telit UE866 UART 1.8 V          <----->          Arduino Nano UART 3.3 V
 * 
 *                              SN74AVC4T245DR
 *                              http://www.ti.com/lit/ds/symlink/sn74avc4t245.pdf
 *                              
 *         Rx                   1DIR Hight   ----->             Tx
 *         Tx                   2DIR Low     <-----             Rx
 *         GND                  GND          <---->             GND
 *                              
*/

#include <avr/power.h>
#include <avr/sleep.h>
#include <Arduino.h>

int sleepStatus = 0;
int count = 0;
int pin = 0;
unsigned long lastReadTime;
float humidity;
float temperature;
char hexmap[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

void sleepNow() {
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();  
  power_adc_disable();
  power_spi_disable();
  power_timer0_disable();
  power_timer1_disable();
  power_timer2_disable();
  power_twi_disable();  
  sleep_mode();
  sleep_disable();
  power_all_enable();
}

int am2305_read( int flg ) {
  unsigned long startTime = millis();
  if ( (unsigned long)(startTime - lastReadTime) < 1999L ) {
    return;
  }
  lastReadTime = startTime;

  // Send start signal
  digitalWrite(pin, LOW);
  pinMode(pin, OUTPUT);
  delayMicroseconds(800);
  pinMode(pin, INPUT);
  // Switch bus to receive data
  digitalWrite(pin, HIGH);

  uint16_t rhumidity = 0;
  uint16_t rtemperature = 0;
  uint16_t data = 0;

  for ( int8_t i = -3 ; i < 2 * 40; i++ ) {
    byte age;
    startTime = micros();
    
    do {
      age = (unsigned long)(micros() - startTime);
      if ( age > 90 ) {
        return 1;
      }
    }
    
    while ( digitalRead(pin) == (i & 1) ? HIGH : LOW );
    
    if ( i >= 0 && (i & 1) ) {
      data <<= 1;

      if ( age > 30 ) {
        data |= 1;
      }
    }
    
    switch ( i ) {
      case 31:
        rhumidity = data;
        break;
      case 63:
        rtemperature = data;
        data = 0;
        break;
    }
  }

  if ( (byte)(((byte)rhumidity) + (rhumidity >> 8) + ((byte)rtemperature) + (rtemperature >> 8)) != data ) {
    return 2;
  }

  if ( flg == 1 ) {
/*    Serial.write((byte)rhumidity);
    Serial.write(rhumidity >> 8);
    Serial.write((byte)rtemperature);
    Serial.write(rtemperature >> 8);
    Serial.write((byte)data);

    Serial.write("\tHex: ");
*/
    Serial.write( hexmap [ ( rhumidity >> 12 ) &0x0f ] );
    Serial.write( hexmap [ ( rhumidity >> 8 ) &0x0f ] );
    Serial.write( hexmap [ ( rhumidity >> 4 ) &0x0f ] );
    Serial.write( hexmap [ ( rhumidity >> 0 ) &0x0f ] );

    Serial.write( hexmap [ ( rtemperature >> 12 ) &0x0f ] );
    Serial.write( hexmap [ ( rtemperature >> 8 ) &0x0f ] );
    Serial.write( hexmap [ ( rtemperature >> 4 ) &0x0f ] );
    Serial.write( hexmap [ ( rtemperature >> 0 ) &0x0f ] );
    
    Serial.write( hexmap [ ( data >> 4 ) &0x0f ] );
    Serial.write( hexmap [ ( data >> 0 ) &0x0f ] );
  }

  humidity = rhumidity * 0.1;
  
  if ( rtemperature & 0x8000 ) {
    rtemperature = -(int16_t)(rtemperature & 0x7FFF);
  }
  temperature = ((int16_t)rtemperature) * 0.1;

  return 0;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  pin = 5;
}

void loop() {
  #if 0
  // put your main code here, to run repeatedly:
  lastReadTime = millis() - 3000; 
  while (1) {
    int res = am2305_read();
    Serial.print("\rStatus: " );
    Serial.print( res, 1 );
    Serial.print("\t\tHumidity: ");
    Serial.print(humidity, 1);
    Serial.print("\t\tTemperature: ");
    Serial.print(temperature, 1);
    Serial.print("\r\n");
    delay( 2000 );
}
#endif

  delay(5);

  if (Serial.available()) {
    int val = Serial.read();

    if (val == 'A') {
      lastReadTime = millis() - 3000; 
      if ( !am2305_read( 0 ) ) {
        Serial.print("Humidity: ");
        Serial.print(humidity, 1);
        Serial.print("\t\tTemperature: ");
        Serial.print(temperature, 1);
        Serial.print("\r\n");
        humidity = 0 ;
        temperature = 0 ;
      }
      delay(5);
    }

    if ( val == 0x7e ) {
      lastReadTime = millis() - 3000; 
      if ( !am2305_read( 1 ) ) {
        humidity = 0 ;
        temperature = 0 ;
      }
      delay(5);
    }
  }

  sleepNow();
}
