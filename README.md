# README #

Telit UE-866 G3 + Arduino Nano + Aosong AM2305 sensor


author:  Denis Shashunkin

email:  d.shashunkin@yandex.ru


## Hardware installation instructions


1. connect Aosong AM2305 and Arduino Nano:

  Aosong AM2305                 Arduino Nano
  
      VCC           <--->         +3.3 v
      
      SDA           <--->         pull-up resistor(4.7 - 5.1kΩ) on pin D5 
      
      GND           <--->         GND

2. connect Arduino Nano and Telit UE-866 G3 over SN74AVC4T245DR:

  Arduino Nano        SN74AVC4T245DR         Telit UE-866 G3
  
      TXd      <_-->   1DIR Hight     <--->  RXD_AUX/SPI_MISO
      
      RXd      <--->    2DIR Low      <--->  TXD_AUX/SPI_MOSI
      
      GND      <--->     GND          <--->       GND


### Software installation instructions

1. download Arduino IDE from https://www.arduino.cc/en/Main/Software
2. install it
3. connect Arduino Nano to PC over usb cable
4. open Windows Device Manager and check what new COM port was assigned


### Upload firmware instructions

1. run arduino_am2305.ino
2. select board Arduino Nano ( Main menu -> Tools -> Board -> select board )
3. select COM port ( Main menu -> Tools -> Port -> select COM port )
4. fix/add new functionality of the source code
5. compile source code ( Main menu -> Sketch -> Verify/Compile )
6. upload firmware to Arduino Nano (Main menu -> Sketch -> Upload/Upload usin programmer )
